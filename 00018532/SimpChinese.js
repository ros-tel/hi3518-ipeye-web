﻿var Translate={
	title:"Digital Video Recorder",
	usr:"Username:",
	pswd:"Password:",
	logi:"please login in!",
	login:"Login",
	dev:"device",
	ch:"CAM ",
	MFt:"MainStream",
	SFt:"SubStream",
	Zoom:"zoom",
	Iris:"Iris",
	Focu:"focus",
	Split1:"View 1",
	Split4:"View 4",
	Split9:"View 9",
	Split16:"View 16",
	Split25:"View 25",
	Split36:"View 36",
	PlayAll:"connect all video",
	StopPlayAll:"close all video",
	mainPlayAll:"playAll(mainStream)",
	subPlayAll:"playAll(subStream)",
	ok:"ok",
	Cancel:"Cancel",
	logout:"Logout",
	autoPrompt:"Auto Prompt",
	version:"Version"
}

var Versions=
{   
    vidioIn:"Video In: ",
    audioIn:"Audio In: ",
    alarmIn:"Alarm In: ",
    alarmOut:"Alarm Output: ",
    buildTime:"Build Date:",
    version:"System: ",
    serialNo:"Serial ID: " ,
    ok:"ok" 
    
}

var Password=
{
	findpsw:"Retrieve Password",
	question1:"Question1: ",
	answer:"Answer: ",
	question2:"Question2: ",
	newpassword:"NewPassword: ",
	confirmpassword:"Confirm: ",
	enteranswer:"Please enter the answer",
	enterpassword:"Please enter a password(Password only Numbers and letters)",
	confirmpsw:"Please confirm the password(Password only Numbers and letters)",
	ok:"Submit",
	Back:"Back"	
}
